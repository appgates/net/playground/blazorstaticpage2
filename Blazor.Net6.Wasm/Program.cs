using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Blazor.Net6.Wasm
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            var js = (IJSInProcessRuntime)builder.Services.BuildServiceProvider().GetRequiredService<IJSRuntime>();

            var appContainerId = "myBlazorApp";
            var appHtmlElement = js.Invoke<IJSObjectReference>("document.getElementById", appContainerId);
            var myParam = await appHtmlElement.InvokeAsync<string>("getAttribute", "myParam");
            var startupArgs = new StartupArgs
            {
                MyParam = myParam
            };

            builder.RootComponents.Add<App>($"#{appContainerId}");
            builder.Services.AddSingleton(startupArgs);
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            await builder.Build().RunAsync();
        }

      
    }
    class StartupArgs
    {
        public string MyParam { get; set; }
    }
}
